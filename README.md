# Ads List

#### 介绍
这是一个很憨憨的广告屏蔽规则

 **本规则包含：** 

 **基础广告屏蔽：** 

广告终结者自带规则：http://sub.adtchrome.com/adt-chinalist-easylist360.txt

卡饭论坛乘风视频广告过滤规则：https://gitee.com/xinggsf/Adblock-Rule/raw/master/mv.txt

 **UBO全家桶：** 

uBlock filters：https://raw.githubusercontent.com/uBlockOrigin/uAssets/master/filters/filters-2020.txt

uBlock filters – Badware risks：https://raw.githubusercontent.com/uBlockOrigin/uAssets/master/filters/badware.txt

uBlock filters – Privacy：https://raw.githubusercontent.com/uBlockOrigin/uAssets/master/filters/privacy.txt

 **隐私：** 

AdGuard Tracking Protection：https://filters.adtidy.org/android/filters/3_optimized.txt

 **恶意网站：** 

Spam404：https://raw.githubusercontent.com/Spam404/lists/master/adblock-list.txt

Online Malicious URL Blocklist：https://gitlab.com/curben/urlhaus-filter/raw/master/urlhaus-filter.txt

 **骚扰：** 

Fanboy's Social Blocking List：https://easylist-downloads.adblockplus.org/fanboy-social.txt

 **其他：** 

I don't care about cookies：https://www.i-dont-care-about-cookies.eu/abp/

CJX's Annoyance List：https://gitee.com/cjx82630/cjxlist/raw/master/cjx-annoyance.txt

 **不同版本区别：**


 **基础版包含：** 


广告终结者自带规则（ChinaList+EasyList）

卡饭论坛乘风视频广告过滤规则

推荐配置图：
![最好使用此配置图的配置](https://images.gitee.com/uploads/images/2020/0810/142906_166d9ffb_7908753.png "基础版配置图")


 **增强版包含：** 


AdGuard Tracking Protection

I don't care about cookies

CJX's Annoyance List

以及基础版全部广告规则

推荐配置图：![最好使用此配置图的配置](https://images.gitee.com/uploads/images/2020/0810/143827_0ba5f1cb_7908753.png "增强版配置图")


 **究极版包括（文件非常大，慎用）：** 


以上全部广告屏蔽规则

推荐配置图：![最好使用此配置图的配置](https://images.gitee.com/uploads/images/2020/0810/145559_bfc331ec_7908753.png "究极版配置图")

**添加广告屏蔽拓展和以及如何导入规则的教程（新手专用）：**

**添加拓展（离线法）：**
如果想添加广告屏蔽拓展，我推荐 Edge 和 Chrome 两款浏览器，拓展我推荐 uBlock Origin

(1)下载 uBlock Origin 谷歌内核离线包，uBlock Origin 离线包可以在以下两个网站下载（点击会直接开始下载）

我的 Gitee 仓库下载（速度很快，需登录）：https://gitee.com/gong_cx/Ads_Rule_List/raw/master/.gitee/uBlock_Origin_v1.29.0.zip

我的 GitLab 仓库下载（速度还行，无需登录）：https://gitlab.com/gong_cx/Ads_Rule_List/-/raw/master/.gitlab/uBlock_Origin_v1.29.0.zip?inline=false

(2)接下来我们要解压下载的压缩包，只需要把里面的“uBlock0.chromium”文件夹解压出来就好

效果图：![效果图.png](https://pic.baixiongz.com/2020/08/12/7d4e27aed62fa.png)

(3)然后，打开拓展管理页面（可在地址栏输入：chrome://extensions）接着，打开“开发者模式”，点击“加载以解压的拓展程序”选项，在弹出的选项框中选中之前解压出来的文件夹，然后点击选择文件夹，即可添加了！！！

选项框：![选项框.png](https://pic.baixiongz.com/2020/08/12/5627e23e4c136.png)

**添加规则：**
(1)首先，添加完拓展后，在地址栏后面会多出一个图标，那就是UBO的拓展图标，点击他，然后点击![3.png](https://pic.baixiongz.com/2020/08/12/4a1559890c59a.png)图片里的按钮，打开UBO的控制面板，再点击“规则列表”选项卡，进入广告屏蔽规则配置界面

(2)点击自定义，输入拓展导入的配置地址，点击“应用更改”然后等待一会，出现规则标题，像这样：![4.png](https://pic.baixiongz.com/2020/08/12/878e20299c086.png)
，然后，其实就差不多了，你还可以把规则改成和推荐配置图一样，以便效果最大化

**规则地址：**

如果要配置规则可以前往一下三个仓库的地址（点稽进入）

码云（国内网站，稳定）：https://gitee.com/gong_cx/Ads_Rule_List/blob/master/All_Version_Ads_Rule.txt

Github（国外网站，不稳定）：https://github.com/Gongcxgithub/Ads_Rule_List/blob/master/All_Version_Ads_Rule.txt

GitLab（国外网站，较稳定）：https://gitlab.com/gong_cx/Ads_Rule_List/-/blob/master/All_Version_Ads_Rule.txt